﻿using Geometry2DExercise.IDataStructure;

namespace Geometry2DExercise.DataStructure
{
    public class Point : IPoint
    {
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X { get; set; }
        public double Y { get; set; }

        public override string ToString()
        {
            return "X: " + X + ", Y: " + Y;
        }
    }
}
