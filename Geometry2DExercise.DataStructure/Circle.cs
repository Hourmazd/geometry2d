﻿using System.Collections.Generic;
using Geometry2DExercise.DataStructure.BaseClasses;
using Geometry2DExercise.DataStructure.Helpers;
using Geometry2DExercise.IDataStructure;
using Geometry2DExercise.IDataStructure.BaseClasses;
using Geometry2DExercise.Shared;

namespace Geometry2DExercise.DataStructure
{
    public class Circle : Shape, ICircle
    {
        #region Constructors

        public Circle()
            : this(string.Empty, 0, 0, 0)
        {
        }

        public Circle(string id, double x, double y, double radious)
            : this(id, new Point(x, y), radious)
        {
            Center = new Point(x, y);
            Radious = radious;
        }

        public Circle(string id, IPoint center, double radious)
            : base(ShapeTypes.Circle, id)
        {
            Center = center;
            Radious = radious;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// radios of circle shape
        /// </summary>
        public double Radious
        {
            get { return _Radious; }
            set { SetValue(ref _Radious, value); }
        }

        /// <summary>
        /// center point of circle shape 
        /// </summary>
        public IPoint Center
        {
            get { return _Center; }
            set { SetValue(ref _Center, value); }
        }

        #endregion

        #region Private Fields

        private double _Radious;
        private IPoint _Center;

        #endregion

        #region Override Members
        
        protected override IPolygon GetPolygon()
        {
            var points = new List<IVector>
            {
                new Vector(ContainingRectangle.Point1.X, ContainingRectangle.Point1.Y),
                new Vector(ContainingRectangle.Point2.X, ContainingRectangle.Point2.Y),
                new Vector(ContainingRectangle.Point3.X, ContainingRectangle.Point3.Y),
                new Vector(ContainingRectangle.Point4.X, ContainingRectangle.Point4.Y)
            };

            var result = new Polygon();
            result.CreatePolygon(points);

            return result;
        }

        protected override IRectangle GetContainingRectanle()
        {
            return new Rectangle(Id + "-CR", Center.X - Radious, Center.Y - Radious, Center.X + Radious, Center.Y + Radious);
        }

        public override bool Contains(IPoint point)
        {
            return SATHelper.Instanse.GetDistanceOfTwoPoints(Center, point) <= Radious;
        }
        
        #endregion
    }
}
