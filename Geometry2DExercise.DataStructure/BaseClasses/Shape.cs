﻿using Geometry2DExercise.DataStructure.Helpers;
using Geometry2DExercise.IDataStructure;
using Geometry2DExercise.IDataStructure.BaseClasses;
using Geometry2DExercise.Shared;

namespace Geometry2DExercise.DataStructure.BaseClasses
{
    public abstract class Shape : DTOBase, IShape
    {
        #region Constructors

        protected Shape(ShapeTypes type, string id)
        {
            Type = type;
            Id = id;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Type of the shape
        /// </summary>
        public ShapeTypes Type { get; }

        /// <summary>
        /// Name of the shape
        /// </summary>
        public string Id
        {
            get
            {
                return _Id;
            }
            set
            {
                SetValue(ref _Id, value);
            }
        }
        
        public IRectangle ContainingRectangle => _ContainingRectangle ?? (_ContainingRectangle = GetContainingRectanle());

        /// <summary>
        /// Geometric polygon of shape
        /// </summary>
        public IPolygon Polygon => _Polygon ?? (_Polygon = GetPolygon());

        #endregion

        #region Public Methods

        public abstract bool Contains(IPoint point);

        public virtual bool Contains(IShape shape)
        {
            return SATHelper.Instanse.IsRectangleInsideAnother(ContainingRectangle.Point1, ContainingRectangle.Point3, shape.ContainingRectangle.Point1, shape.ContainingRectangle.Point3);
        }

        public virtual bool Overlap(IShape shape)
        {
            return SATHelper.Instanse.CheckSeparatingAxisOverlap(this, shape);
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// generate the geometric polygon of shape
        /// </summary>
        /// <returns></returns>
        protected abstract IPolygon GetPolygon();
        
        protected abstract IRectangle GetContainingRectanle();

        #endregion
        
        #region Private Fields

        private string _Id;
        private IPolygon _Polygon;
        private IRectangle _ContainingRectangle;

        #endregion

        #region Override Methods

        public override bool Equals(object obj)
        {
            return obj != null && ((Shape)obj).Id == this.Id;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return Id;
        }

        #endregion
    }
}
