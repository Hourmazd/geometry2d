using System.Collections.Generic;
using Geometry2DExercise.IDataStructure.BaseClasses;

namespace Geometry2DExercise.DataStructure.BaseClasses
{
    public class Polygon : IPolygon
    {
        #region Constructors

        public Polygon()
        {
            Edges = new List<IVector>();
        }

        #endregion

        #region Public Properties

        public IList<IVector> Edges { get; }

        public IList<IVector> Points { get; private set; }

        #endregion

        #region Public Methods

        public void CreatePolygon(IList<IVector> points)
        {
            Points = points;

            for (var i = 0; i < Points.Count; i++)
            {
                var p1 = Points[i];
                var p2 = i + 1 >= Points.Count ? Points[0] : Points[i + 1];

                Edges.Add((Vector)p2 - (Vector)p1);
            }
        }

        #endregion
    }
}

