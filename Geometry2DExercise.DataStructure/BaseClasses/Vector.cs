using System;
using Geometry2DExercise.IDataStructure.BaseClasses;

namespace Geometry2DExercise.DataStructure.BaseClasses
{
    public struct Vector : IVector
    {
        #region Constructors

        public Vector(double x, double y)
        {
            X = x;
            Y = y;
        }

        #endregion

        #region Public Properties

        public double X { get; set; }
        public double Y { get; set; }
        public double Magnitude => Math.Sqrt(X * X + Y * Y);

        #endregion

        #region Public Methods

        public void Normalize()
        {
            var magnitude = Magnitude;
            X = X / magnitude;
            Y = Y / magnitude;
        }

        public double DotProduct(IVector vector)
        {
            return X * vector.X + Y * vector.Y;
        }

        #endregion

        #region Operator Refactoring

        public static IVector operator -(Vector a)
        {
            return new Vector(-a.X, -a.Y);
        }

        public static IVector operator -(Vector a, Vector b)
        {
            return new Vector(a.X - b.X, a.Y - b.Y);
        }

        #endregion

        public override string ToString()
        {
            return "X: " + X + ", Y: " + Y + ", M: " + Magnitude;
        }
    }
}
