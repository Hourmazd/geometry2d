﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using Geometry2DExercise.IDataStructure.BaseClasses;
using Geometry2DExercise.Shared;

namespace Geometry2DExercise.DataStructure.BaseClasses
{
    public abstract class DTOBase : IDTOBase
    {
        #region Constructors
        #endregion

        #region Public Events

        public event EventHandler OnStatusChanged;
        public event EventHandler OnValueChanged;

        #endregion

        #region Public Properties

        public ObjectStates State
        {
            get { return _ObjectState; }
            private set
            {
                if (_ObjectState != value)
                {
                    _ObjectState = value;
                    StatusChanged(this, null);
                }
            }
        }

        public virtual bool isDirty
        {
            get { return (State == ObjectStates.Added || State == ObjectStates.Modified || State == ObjectStates.Removed); }
        }

        public bool IsNew
        {
            get { return (State == ObjectStates.Added || State == ObjectStates.AddedRemoved); }
        }

        #endregion

        #region Public Methods

        public virtual void SetRemoved()
        {
            switch (State)
            {
                case ObjectStates.Added:
                    State = ObjectStates.AddedRemoved;
                    break;
                case ObjectStates.AddedRemoved:
                case ObjectStates.Removed:
                    //Nothing
                    break;
                case ObjectStates.Modified:
                case ObjectStates.None:
                    State = ObjectStates.Removed;
                    break;
            }
        }

        public void SetAdded()
        {
            State = ObjectStates.Added;
        }

        public void SetDirty()
        {
            ValueChanged();
        }

        public virtual void ResetFlages()
        {
            State = ObjectStates.None;
        }

        public void ResetRemoved()
        {
            if (State == ObjectStates.Removed)
            {
                State = ObjectStates.None;
            }
            if (State == ObjectStates.AddedRemoved)
            {
                State = ObjectStates.Added;
            }
        }

        public virtual IDTOBase Clone()
        {
            return (IDTOBase)MemberwiseClone();
        }

        public string SerializeToXML()
        {
            try
            {
                var xmlSer = new XmlSerializer(GetType());
                var sWriter = new StringWriter();

                // Serialize the dto to xml.
                xmlSer.Serialize(sWriter, this);

                return sWriter.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in XML serialization", ex);
            }
        }

        public void BeginInitiate()
        {
            _IsInitiating = false;
        }

        public void EndInitiate()
        {
            _IsInitiating = true;
        }

        #endregion

        #region Protected Methods

        protected bool SetValue<PropertyType>(string inSourcePropertyName, ref PropertyType destination, PropertyType newValue/*, bool inForcePropertyModify*/)
        {
            var result = false;

            try
            {
                if ((newValue != null && !newValue.Equals(destination)) || (destination != null && !destination.Equals(newValue)))
                {
                    destination = newValue;

                    ValueChanged();

                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error in setting value data.", ex);
            }

            return result;
        }

        protected bool SetValue<ObjectType>(ref ObjectType destination, ObjectType newValue)
        {
            string propertyName;

            try
            {
                var parameters = new StackTrace().GetFrame(1).GetMethod().GetParameters();
                propertyName = parameters[0].Member.Name.Replace("set_", string.Empty);
            }
            catch (Exception)
            {
                throw new Exception("Error in setting property value.");
            }

            return SetValue(propertyName, ref destination, newValue);
        }

        protected void ValueChanged(object sender, EventArgs e)
        {
            ValueChanged();
        }

        protected void ValueChanged()
        {
            if (!_IsInitiating)
            {
                if (State == ObjectStates.None)
                {
                    State = ObjectStates.Modified;
                }

                if (OnValueChanged != null)
                {
                    OnValueChanged(this, null);
                }
            }
        }

        protected void StatusChanged(object sender, EventArgs e)
        {
            if (!_IsInitiating && OnStatusChanged != null)
            {
                OnStatusChanged(sender, e);
            }
        }

        #endregion

        #region Private Fields

        private ObjectStates _ObjectState = ObjectStates.None;
        private bool _IsInitiating = false;

        #endregion
    }
}
