﻿using System;
using System.Collections.Generic;
using Geometry2DExercise.DataStructure.BaseClasses;
using Geometry2DExercise.IDataStructure;
using Geometry2DExercise.IDataStructure.BaseClasses;

namespace Geometry2DExercise.DataStructure.Helpers
{
    public class SATHelper
    {
        #region Constructors

        private SATHelper()
        {
        }

        #endregion

        #region Static Members

        private static SATHelper _Instanse;

        public static SATHelper Instanse
        {
            get
            {
                if (_Instanse == null)
                    throw new Exception("There is not any live instance of Helper. Call 'Start' method to generate new instance.");

                return _Instanse;
            }
        }

        public static void Start()
        {
            if (_Instanse == null)
                _Instanse = new SATHelper();
        }

        #endregion

        #region Public Properties
        #endregion

        #region Public Methods
        
        public double GetDistanceOfTwoPoints(IPoint point1, IPoint point2)
        {
            return Math.Sqrt(Math.Pow(point2.X - point1.X, 2) + Math.Pow(point2.Y - point1.Y, 2));
        }

        public double CalculateAreaOfTriangle(IPoint point1, IPoint point2, IPoint point3)
        {
            return Math.Abs((point1.X * (point2.Y - point3.Y) + point2.X * (point3.Y - point1.Y) + point3.X * (point1.Y - point2.Y)) / 2.0);
        }

        public bool IsRectangleInsideAnother(IPoint R1P1, IPoint R1P3, IPoint R2P1, IPoint R2P3)
        {
            return R1P1.X <= R2P1.X && R1P1.Y <= R2P1.Y && R1P3.X >= R2P3.X && R1P3.Y >= R2P3.Y;
        }
        
        public bool CheckSeparatingAxisOverlap(IShape shape1, IShape shape2)
        {
            var result = true;
            var edges = new List<IVector>();
            edges.AddRange(shape1.Polygon.Edges);
            edges.AddRange(shape2.Polygon.Edges);

            // Loop through all the edges of both polygons
            foreach (var edge in edges)
            {
                // Find the axis perpendicular to the current edge
                var axis = new Vector(-edge.Y, edge.X);
                axis.Normalize();

                // Find the projection of the polygon on the current axis
                double minA = 0;
                double minB = 0;
                double maxA = 0;
                double maxB = 0;

                ProjectPolygon(axis, shape1.Polygon, ref minA, ref maxA);
                ProjectPolygon(axis, shape2.Polygon, ref minB, ref maxB);

                // Check if the polygon projections are currentlty intersecting
                if (GetProjectionDistance(minA, maxA, minB, maxB) > 0)
                    result = false;

                if (!result) break;
            }

            return result;
        }

        // Calculate the distance between [minA, maxA] and [minB, maxB]
        // The distance will be negative if the intervals overlap
        public double GetProjectionDistance(double minA, double maxA, double minB, double maxB)
        {
            if (minA < minB)
            {
                return minB - maxA;
            }
            else
            {
                return minA - maxB;
            }
        }

        // Calculate the projection of a polygon on an axis and returns it as a [min, max] interval
        public void ProjectPolygon(IVector axis, IDataStructure.BaseClasses.IPolygon polygon, ref double min, ref double max)
        {
            // To project a point on an axis use the dot product
            var d = axis.DotProduct(polygon.Points[0]);
            min = d;
            max = d;

            foreach (var p in polygon.Points)
            {
                d = p.DotProduct(axis);

                if (d < min)
                    min = d;
                else if (d > max)
                    max = d;
            }
        }

        #endregion
    }
}
