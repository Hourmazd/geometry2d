﻿using System.Collections.Generic;
using Geometry2DExercise.DataStructure.BaseClasses;
using Geometry2DExercise.IDataStructure;
using Geometry2DExercise.IDataStructure.BaseClasses;
using Geometry2DExercise.Shared;


namespace Geometry2DExercise.DataStructure
{
    public class Rectangle : Shape, IRectangle
    {
        #region Constructors

        public Rectangle()
            : this(string.Empty, 0, 0, 0, 0)
        {
        }

        public Rectangle(string id, double x1, double y1, double x3, double y3)
            : this(id, new Point(x1, y1), new Point(x3, y3))
        {
        }

        public Rectangle(string id, IPoint point1, IPoint point3)
            : base(ShapeTypes.Rectangle, id)
        {
            Point1 = point1;
            Point3 = point3;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// bottom left point of rectangle shape
        /// </summary>
        public IPoint Point1
        {
            get { return _Point1; }
            set { SetValue(ref _Point1, value); }
        }

        /// <summary>
        /// top left point of rectangle shape
        /// </summary>
        public IPoint Point2 => new Point(Point1.X, Point3.Y);

        /// <summary>
        /// top right point of rectangle shape
        /// </summary>
        public IPoint Point3
        {
            get { return _Point3; }
            set { SetValue(ref _Point3, value); }
        }

        /// <summary>
        /// bottom right point of rectangle shape
        /// </summary>
        public IPoint Point4 => new Point(Point3.X, Point1.Y);

        #endregion

        #region Private Fields

        private IPoint _Point1;
        private IPoint _Point3;

        #endregion

        #region Override Members
        
        protected override IPolygon GetPolygon()
        {
            var points = new List<IVector>
            {
                new Vector(Point1.X, Point1.Y),
                new Vector(Point2.X, Point2.Y),
                new Vector(Point3.X, Point3.Y),
                new Vector(Point4.X, Point4.Y)
            };

            var result = new Polygon();
            result.CreatePolygon(points);

            return result;
        }

        protected override IRectangle GetContainingRectanle()
        {
            return new Rectangle(Id + "-CR", Point1, Point3);
        }

        public override bool Contains(IPoint point)
        {
            return point.X >= Point1.X && point.X <= Point3.X && point.Y >= Point1.Y && point.Y <= Point3.Y;
        }
        
        #endregion
    }
}
