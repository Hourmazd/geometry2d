﻿using System;
using System.Collections.Generic;
using Geometry2DExercise.DataStructure.BaseClasses;
using Geometry2DExercise.DataStructure.Helpers;
using Geometry2DExercise.IDataStructure;
using Geometry2DExercise.IDataStructure.BaseClasses;
using Geometry2DExercise.Shared;

namespace Geometry2DExercise.DataStructure
{
    public class Triangle : Shape, ITriangle
    {
        #region Constructors

        public Triangle()
            : this(string.Empty, 0, 0, 0, 0, 0, 0)
        {
        }

        public Triangle(string id, double x1, double y1, double x2, double y2, double x3, double y3)
            : this(id, new Point(x1, y1), new Point(x2, y2), new Point(x3, y3))
        {
        }

        public Triangle(string id, IPoint point1, IPoint point2, IPoint point3)
            : base(ShapeTypes.Triangle, id)
        {
            Point1 = point1;
            Point2 = point2;
            Point3 = point3;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// first point of the triangle shape
        /// </summary>
        public IPoint Point1
        {
            get { return _Point1; }
            set { SetValue(ref _Point1, value); }
        }

        /// <summary>
        /// second point of the triangle shape
        /// </summary>
        public IPoint Point2
        {
            get { return _Point2; }
            set { SetValue(ref _Point2, value); }
        }

        /// <summary>
        /// third point of the triangle shape
        /// </summary>
        public IPoint Point3
        {
            get { return _Point3; }
            set { SetValue(ref _Point3, value); }
        }

        #endregion

        #region Private Fields

        private IPoint _Point1;
        private IPoint _Point2;
        private IPoint _Point3;

        #endregion

        #region Override Members
        
        protected override IDataStructure.BaseClasses.IPolygon GetPolygon()
        {
            var points = new List<IVector>
            {
                new Vector(Point1.X, Point1.Y),
                new Vector(Point2.X, Point2.Y),
                new Vector(Point3.X, Point3.Y)
            };

            var result = new Polygon();
            result.CreatePolygon(points);

            return result;
        }

        public override bool Contains(IPoint point)
        {
            var fullArea = SATHelper.Instanse.CalculateAreaOfTriangle(Point1, Point2, Point3);
            var area1 = SATHelper.Instanse.CalculateAreaOfTriangle(point, Point2, Point3);
            var area2 = SATHelper.Instanse.CalculateAreaOfTriangle(Point1, point, Point3);
            var area3 = SATHelper.Instanse.CalculateAreaOfTriangle(Point1, Point2, point);

            return fullArea == area1 + area2 + area3;
        }

        protected override IRectangle GetContainingRectanle()
        {
            var maxX = Math.Max(Math.Max(Point1.X, Point2.X), Point3.X);
            var maxY = Math.Max(Math.Max(Point1.Y, Point2.Y), Point3.Y);
            var minX = Math.Min(Math.Min(Point1.X, Point2.X), Point3.X);
            var minY = Math.Min(Math.Min(Point1.Y, Point2.Y), Point3.Y);

            return  new Rectangle(Id + "-CR", minX, minY, maxX, maxY);
        }

        #endregion
    }
}
