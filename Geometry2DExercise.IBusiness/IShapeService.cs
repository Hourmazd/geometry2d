﻿using System.Collections.Generic;
using Geometry2DExercise.IDataStructure;

namespace Geometry2DExercise.IBusiness
{
    public interface IShapeService: IServiceBase
    {
        IEnumerable<IShape> GetShapesFromExcell();
        IEnumerable<IShape> GetShapesFromExcell(string directory);
        IEnumerable<IShape> GetShapesAtSpecificPoint(string directory, IPoint point);
        IEnumerable<IShape> GetShapesAtSpecificPoint(string directory, double x, double y);
        IEnumerable<IShape> GetShapesInsideSpecificRectangle(string directory, IPoint point1, IPoint point3);
        IEnumerable<IShape> GetShapesInsideSpecificRectangle(string directory, double x1, double y1, double x3, double y3);
        IDictionary<IShape, IEnumerable<IShape>> GetShapesOverlapping(string directory);
    }
}
