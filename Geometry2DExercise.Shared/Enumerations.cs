﻿namespace Geometry2DExercise.Shared
{
    public enum ObjectStates
    {
        None,
        Added,
        Modified,
        Removed,
        AddedRemoved
    }

    public enum ShapeTypes
    {
        Circle,
        Rectangle,
        Triangle
    }

    public enum Actions
    {
        OBJECTS_AT_POINT,
        INSIDE_RECTANGLE,
        OVERLAPPING_OBJECTS
    }
}
