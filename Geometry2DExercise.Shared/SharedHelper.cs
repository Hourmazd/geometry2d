﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Geometry2DExercise.Shared
{
    public class SharedHelper
    {
        #region Constructors

        private SharedHelper()
        {
            PI = Math.PI;
        }

        #endregion

        #region Static Members

        private static SharedHelper _Instanse;

        public static SharedHelper Instanse
        {
            get
            {
                if (_Instanse == null)
                    throw new Exception("There is not any live instance of Helper. Call 'Start' method to generate new instance.");

                return _Instanse;
            }
        }

        public static void Start()
        {
            if (_Instanse == null)
                _Instanse = new SharedHelper();
        }

        #endregion

        #region Public Properties

        public double PI;

        #endregion

        #region Public Methods

        public string GetExecutionPath()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        public string GetStaticImportDirectory()
        {
            return Directory.GetParent(GetExecutionPath()).Parent.FullName + "\\SampleData";
        }

        public void GetPointFromArgument(string argument, ref double x, ref double y)
        {
            string arg = argument.Trim();

            if (!arg.StartsWith("(") || !arg.EndsWith(")"))
                throw new Exception("Point argument should be between ( and )");

            arg = arg.Replace("(", string.Empty).Replace(")", String.Empty);
            
            if (!double.TryParse(arg.Split(',').First(), out x))
                throw new Exception("X value of point '" + arg + "' is in wrong format.");


            if (!double.TryParse(arg.Split(',').Last(), out y))
                throw new Exception("Y value of point '" + arg + "' is in wrong format.");
        }

        public string RealizeDirectory(string source)
        {
            if (string.IsNullOrEmpty(source))
                return string.Empty;

            if (Regex.IsMatch(source, "[A-Z]:|[a-z]:"))
                return source;

            var backwardCount = 0;
            while (source.StartsWith("..\\"))
            {
                backwardCount++;
                source = source.Remove(0, 3);
            }

            var execPath = GetExecutionPath();

            while (backwardCount >= 0)
            {
                backwardCount--;
                execPath = Directory.GetParent(execPath).FullName;
            }

            if (!source.StartsWith("\\") && !execPath.EndsWith("\\"))
                source = "\\" + source;

            return execPath + source;
        }

        #endregion
    }
}
