﻿

The task is to create a program that selects geometric 2D objects that
match a given criteria from a dataset. The type of objects are Circle, 
Rectangle and Triangle.

The dataset is stored as comma separated values (csv) files. In the 
directory SampleData you will find some test data. One csv-file will 
contain only one type of objects.

The number of geometric objects could be larger than in the example 
dataset. You can assume that the whole dataset will fit into memory.


Command line syntax:
Geometry.exe [directory] [operation] [input_for_the_operation]

where 
  directory: is the absolute or relative path to a directory containing csv files (e.g. SampleData directory)

  operation: OBJECTS_AT_POINT, INSIDE_RECTANGLE, or OVERLAPPING_OBJECTS

  input_for_the_operation: 
         OBJECTS_AT_POINT takes one point in the format (x,y), for example (10.2,-3.4)
		 INSIDE_RECTANGLE takes two points (x1,y1) (x2,y2)
		 OVERLAPPING_OBJECTS takes no input




1. Print the ID of all objects at a given point. If the given point is at the border 
   of an object, then it is considered a hit.  
Example:

Geometry.exe c:\temp\SampleData OBJECTS_AT_POINT (10.3,7.4)

Objects at point (10.3,7.4):
C1
C14
C3
R3
T5

----------------------------------------------------------------

2. For each object, output the ID of all objects that they are overlapping

Geometry.exe c:\temp\SampleData OVERLAPPING_OBJECTS

C1 is overlapping: C2, C4, T7
C3 is overlapping:
C4 is overlapping: C1
...


The use of open source libraries (NuGet packages) is allowed for 
 - parsing the CSV files,
 - maths and geometry libraries.

Libraries that do 2D-collision checks are not allowed.

If you use some code you find online, please add a comment with the URL to the page. 

Please do not post the exercise online, as we reuse this same exercises 
for multiple candidates.


Please pay attention to:
- Clarity of the code.
- Design and structure of the program
- Correctness and robustness
- Performance, the code should be fast.


Programming language: C#

If you have questions regarding the exercise, please do not hesitate to ask.
