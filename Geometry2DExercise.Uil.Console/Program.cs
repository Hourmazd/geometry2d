﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Geometry2DExercise.Shared;
using Geometry2DExercise.Factory;
using Geometry2DExercise.IBusiness;
using Geometry2DExercise.IDataStructure;
using Geometry2DExercise.DataStructure;

namespace Geometry2DExercise.Uil.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var prm = new string[2];
            prm[0] = "..\\SampleData";
            prm[1] = Actions.OVERLAPPING_OBJECTS.ToString();
            //prm[2] = "(10,10)";
            //prm[3] = "(50,50)";

            //Since BolFactory is a base instace factory of BOL classes in system, should starts at the first step of runtime
            BolFactory.Start();

            //Getting an instance of ShapeService
            var service = BolFactory.Instanse.GetShapeService();
            var param = GetCommandParameters(prm);
            ExecuteCommand(service, param);
            System.Console.ReadKey();
        }

        private static void ExecuteCommand(IShapeService service, ExecutionParameters executionParams)
        {
            switch (executionParams.Action)
            {
                case Actions.OBJECTS_AT_POINT:

                    var actionResult1 = service.GetShapesAtSpecificPoint(executionParams.Directory, executionParams.Points.First());

                    System.Console.WriteLine();
                    System.Console.WriteLine("Shapes at point X:" + executionParams.Points.First().X.ToString("F") + ", Y: " + executionParams.Points.First().Y.ToString("F"));
                    System.Console.WriteLine("-------------------------------------------------------------------------------------");
                    foreach (var shape in actionResult1)
                        System.Console.WriteLine(shape);

                    break;

                case Actions.INSIDE_RECTANGLE:

                    var actionResult2 = service.GetShapesInsideSpecificRectangle(executionParams.Directory, executionParams.Points.First(), executionParams.Points.Last());
                    System.Console.WriteLine();
                    System.Console.WriteLine("Shapes incide the rectangle (" + executionParams.Points.First().X.ToString("F") + ", " + executionParams.Points.First().Y.ToString("F") +
                        ") and (" + executionParams.Points.Last().X.ToString("F") + ", " + executionParams.Points.Last().Y.ToString("F") + ")");
                    System.Console.WriteLine("-------------------------------------------------------------------------------------");
                    foreach (var shape in actionResult2)
                        System.Console.WriteLine(shape);

                    break;

                case Actions.OVERLAPPING_OBJECTS:
                    var actionResult3 = service.GetShapesOverlapping(executionParams.Directory);

                    foreach (var shape in actionResult3.Keys)
                    {
                        System.Console.Write(shape + " is overlapping: ");

                        foreach (var innershape in actionResult3[shape])
                            System.Console.Write(innershape + ", ");

                        System.Console.WriteLine();
                        System.Console.WriteLine("-------------------------------------------------------------------------------------");
                    }

                    System.Console.ReadKey();
                    break;
            }
        }

        /// <summary>
        /// translating the command line arguments to 'ExecutionParameters'
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static ExecutionParameters GetCommandParameters(string[] args)
        {
            var directory = SharedHelper.Instanse.RealizeDirectory(args[0].Trim());
            if (string.IsNullOrEmpty(directory) || !Directory.Exists(directory))
            {
                System.Console.WriteLine("Input value for directory is not valid. Please try again.");
                Environment.Exit(0);
            }

            Actions action;
            if (!Enum.TryParse(args[1].Trim(), true, out action))
            {
                System.Console.WriteLine("Input value for action is not valid. Please try again.");
                Environment.Exit(0);
            }

            IPoint point1 = null;
            IPoint point2 = null;

            if (action == Actions.INSIDE_RECTANGLE)
            {
                if (args.Length != 4)
                {
                    System.Console.WriteLine("Action '" + Actions.INSIDE_RECTANGLE + "' import directory and needs 2 points. Please try again.");
                    Environment.Exit(0);
                }

                double x1 = 0, x2 = 0, y1 = 0, y2 = 0;
                SharedHelper.Instanse.GetPointFromArgument(args[2], ref x1, ref y1);
                SharedHelper.Instanse.GetPointFromArgument(args[3], ref x2, ref y2);

                point1 = new Point(x1, y1);
                point2 = new Point(x2, y2);
            }
            else if (action == Actions.OBJECTS_AT_POINT)
            {
                if (args.Length != 3)
                {
                    System.Console.WriteLine("Action '" + Actions.OBJECTS_AT_POINT + "' import directory and needs 1 point. Please try again.");
                    Environment.Exit(0);
                }

                double x1 = 0, y1 = 0;
                SharedHelper.Instanse.GetPointFromArgument(args[2], ref x1, ref y1);
                point1 = new Point(x1, y1);
            }
            else if (action == Actions.OVERLAPPING_OBJECTS && args.Length != 2)
            {
                System.Console.WriteLine("Action '" + Actions.OVERLAPPING_OBJECTS + "' just needs import directory. Please try again.");
                Environment.Exit(0);
            }

            return new ExecutionParameters()
            {
                Action = action,
                Directory = directory,
                Points = new List<IPoint> { point1, point2 }
            };
        }
    }

    internal struct ExecutionParameters
    {
        public Actions Action;
        public string Directory;
        public IList<IPoint> Points;
    }
}
