﻿using System.Collections.Generic;
using System.Linq;
using Geometry2DExercise.Business.Helpers;
using Geometry2DExercise.DataStructure;
using Geometry2DExercise.IBusiness;
using Geometry2DExercise.IDataStructure;
using Geometry2DExercise.Shared;

namespace Geometry2DExercise.Business
{
    public class ShapeService : ServiceBase, IShapeService
    {
        #region Constructors

        public ShapeService()
        {
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Import shapes from source excell file
        /// </summary>
        /// <param name="directory">source directory of excell data files</param>
        /// <returns>List of shapes</returns>
        public IEnumerable<IShape> GetShapesFromExcell(string directory)
        {
            return IOHelper.Instanse.ReadShapesFromDirectory(directory);
        }

        /// <summary>
        /// Import shapes from source excell file - source directory will be >> ExecutionPath/SampleData
        /// </summary>
        /// <returns>List of shapes</returns>
        public IEnumerable<IShape> GetShapesFromExcell()
        {
            return GetShapesFromExcell(SharedHelper.Instanse.GetStaticImportDirectory());
        }

        /// <summary>
        /// get the list of objects that contains or touch a point of (x, y)
        /// </summary>
        /// <param name="directory">source directory of excell data files</param>
        /// <param name="point">checking point</param>
        /// <returns></returns>
        public IEnumerable<IShape> GetShapesAtSpecificPoint(string directory, IPoint point)
        {
            var shapes = GetShapesFromExcell(directory);
            return shapes.Where(e => e.Contains(point));
        }

        /// <summary>
        /// get the list of objects that contains or touch a point of (x, y)
        /// </summary>
        /// <param name="directory">source directory of excell data files</param>
        /// <param name="x">x dimention of cheching point</param>
        /// <param name="y">y dimention of cheching point</param>
        /// <returns></returns>
        public IEnumerable<IShape> GetShapesAtSpecificPoint(string directory, double x, double y)
        {
            return GetShapesAtSpecificPoint(directory, new Point(x, y));
        }

        /// <summary>
        /// get the list of objects inside a rectangle
        /// </summary>
        /// <param name="directory">source directory of excell data files</param>
        /// <param name="point1">buttom left point of rectangle</param>
        /// <param name="point3">top right point of rectangle</param>
        /// <returns></returns>
        public IEnumerable<IShape> GetShapesInsideSpecificRectangle(string directory, IPoint point1, IPoint point3)
        {
            var envlope = new Rectangle("R0", point1, point3);
            var shapes = GetShapesFromExcell(directory);
            return shapes.Where(e => envlope.Contains(e));
        }

        /// <summary>
        /// get the list of objects inside a rectangle
        /// </summary>
        /// <param name="directory">source directory of excell data files</param>
        /// <param name="x1">x dimention buttom left point of rectangle</param>
        /// <param name="y1">y dimention buttom left point of rectangle</param>
        /// <param name="x3">x dimention top right point of rectangle</param>
        /// <param name="y3">y dimention top right point of rectangle</param>
        /// <returns></returns>
        public IEnumerable<IShape> GetShapesInsideSpecificRectangle(string directory, double x1, double y1, double x3, double y3)
        {
            return GetShapesInsideSpecificRectangle(directory, new Point(x1, y1), new Point(x3, y3));
        }

        /// <summary>
        /// get the list of objects that has overlapping with other objects
        /// </summary>
        /// <param name="directory">source directory of excell data files</param>
        /// <returns></returns>
        public IDictionary<IShape, IEnumerable<IShape>> GetShapesOverlapping(string directory)
        {
            var shapes = GetShapesFromExcell(directory);

            return shapes.ToDictionary(shape => shape, shape => shapes.Where(e => e != shape && shape.Overlap(e)));
        }

        #endregion
    }
}
