﻿using Geometry2DExercise.IBusiness;

namespace Geometry2DExercise.Business
{
    public abstract class ServiceBase : IServiceBase
    {
        #region Constructors

        protected ServiceBase()
        {
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Any required action for initiating the service
        /// </summary>
        public virtual void InitService()
        {
        }

        #endregion
    }
}
