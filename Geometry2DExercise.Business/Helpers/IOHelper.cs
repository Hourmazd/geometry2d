﻿using Geometry2DExercise.DataStructure.BaseClasses;
using Geometry2DExercise.IDataStructure;
using Geometry2DExercise.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Geometry2DExercise.DataStructure;

namespace Geometry2DExercise.Business.Helpers
{
    public class IOHelper
    {
        #region Constructors

        private IOHelper()
        {
            _Result = new List<Shape>();
        }

        #endregion

        #region Static Members

        private static IOHelper _Instanse;

        public static IOHelper Instanse
        {
            get
            {
                if (_Instanse == null)
                    throw new Exception("There is not any live instance of IOHelper. Call 'Start' method to generate new instance.");

                return _Instanse;
            }
        }

        public static void Start()
        {
            if (_Instanse == null)
                _Instanse = new IOHelper();
        }

        #endregion

        #region Public Methods

        public IEnumerable<IShape> ReadShapesFromDirectory(string directory)
        {
            var result = new List<IShape>();
            var files = Directory.GetFiles(directory, "*.csv");

            foreach (var file in files)
                result.AddRange(ReadShapesFromFile(directory, Path.GetFileName(file)));

            return result;
        }

        #endregion

        #region Private Fields

        private ExcelImporter _ExcelImporter;
        private IList<Shape> _Result;

        #endregion

        #region Private Methods

        private IEnumerable<IShape> ReadShapesFromFile(string directory, string file)
        {
            _Result = new List<Shape>();
            _ExcelImporter = new ExcelImporter();
            _ExcelImporter.OnRecordRead += _ExcelImporter_OnRecordRead;
            _ExcelImporter.ImportExcell(directory, file);
            return _Result;
        }

        private void _ExcelImporter_OnRecordRead(object sender, IOEventArgs e)
        {
            if (!e.Result.Any())
                return;

            var values = e.Result.ToList();
            ShapeTypes shapeType;

            if (!Enum.TryParse(values[0].ToString(), true, out shapeType))
                return;

            Shape resultShape;

            switch (shapeType)
            {
                case ShapeTypes.Circle:
                    resultShape = new Circle(
                        values[1].ToString(),
                        Convert.ToDouble(values[2]),
                        Convert.ToDouble(values[3]),
                        Convert.ToDouble(values[4]));
                    break;

                case ShapeTypes.Rectangle:
                    resultShape = new Rectangle(
                        values[1].ToString(),
                        Convert.ToDouble(values[2]),
                        Convert.ToDouble(values[3]),
                        Convert.ToDouble(values[4]),
                        Convert.ToDouble(values[5]));
                    break;
                case ShapeTypes.Triangle:
                    resultShape = new Triangle(
                        values[1].ToString(),
                        Convert.ToDouble(values[2]),
                        Convert.ToDouble(values[3]),
                        Convert.ToDouble(values[4]),
                        Convert.ToDouble(values[5]),
                        Convert.ToDouble(values[6]),
                        Convert.ToDouble(values[7]));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            _Result.Add(resultShape);
        }

        #endregion
    }
}
