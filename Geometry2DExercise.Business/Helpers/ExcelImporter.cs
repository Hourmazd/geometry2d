﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;

namespace Geometry2DExercise.Business.Helpers
{
    public class ExcelImporter
    {
        #region Event Handller

        public event EventHandler<IOEventArgs> OnRecordRead;

        #endregion

        #region Public Methods

        public void ImportExcell(string directory, string fileName)
        {
            var connectionString = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + directory + ";Extensions=asc,csv,tab,txt";

            using (OdbcConnection connection = new OdbcConnection(connectionString))
            {
                using (OdbcCommand cmd = new OdbcCommand("SELECT * FROM " + fileName, connection))
                {
                    connection.Open();

                    using (OdbcDataReader dr = cmd.ExecuteReader(CommandBehavior.SequentialAccess))
                        while (dr.Read())
                        {
                            var row = new List<object>();

                            for (int i = 0; i < dr.FieldCount; i++)
                                row.Add(dr[i]);

                            RecordRead(row);
                        }

                    connection.Close();
                }
            }
        }

        #endregion

        #region Private Methods

        private void RecordRead(IEnumerable<object> data)
        {
            OnRecordRead?.Invoke(this, new IOEventArgs(data));
        }

        #endregion
    }

    public class IOEventArgs : EventArgs
    {
        public IEnumerable<object> Result { get; private set; }

        public IOEventArgs(IEnumerable<object> result)
        {
            Result = result;
        }
    }
}
