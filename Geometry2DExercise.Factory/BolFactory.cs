﻿using Geometry2DExercise.Business.Helpers;
using Geometry2DExercise.Shared;
using System;
using Geometry2DExercise.Business;
using Geometry2DExercise.DataStructure.Helpers;
using Geometry2DExercise.IBusiness;

namespace Geometry2DExercise.Factory
{
    public class BolFactory
    {
        #region Constructors

        private BolFactory()
        {
            SharedHelper.Start();
            IOHelper.Start();
            SATHelper.Start();
        }

        #endregion

        #region Static Members

        private static BolFactory _Instanse;

        public static BolFactory Instanse
        {
            get
            {
                if (_Instanse == null)
                    throw new Exception("There is not any live instance of Factory. Call 'Start' method to generate new instance.");

                return _Instanse;
            }
        }

        public static void Start()
        {
            if (_Instanse == null)
                _Instanse = new BolFactory();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// create and initiate an instance of 'IShapeService'
        /// </summary>
        /// <returns></returns>
        public IShapeService GetShapeService()
        {
            var service = new ShapeService();
            service.InitService();
            return service;
        }

        #endregion
    }
}
