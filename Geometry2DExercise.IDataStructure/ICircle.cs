﻿namespace Geometry2DExercise.IDataStructure
{
    public interface ICircle : IShape
    {
        IPoint Center { get; set; }
        double Radious { get; set; }
    }
}
