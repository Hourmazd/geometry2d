﻿
namespace Geometry2DExercise.IDataStructure
{
    public interface IPoint
    {
        double X { get; set; }
        double Y { get; set; }
    }
}
