﻿using Geometry2DExercise.IDataStructure.BaseClasses;
using Geometry2DExercise.Shared;

namespace Geometry2DExercise.IDataStructure
{
    public interface IShape : IDTOBase
    {
        ShapeTypes Type { get; }

        string Id { get; set; }

        IRectangle ContainingRectangle { get; }
        
        IPolygon Polygon { get; }
        
        bool Contains(IPoint point);

        bool Contains(IShape shape);

        bool Overlap(IShape shape);
    }
}