﻿namespace Geometry2DExercise.IDataStructure
{
    public interface ITriangle : IShape
    {
        IPoint Point1 { get; set; }
        IPoint Point2 { get; set; }
        IPoint Point3 { get; set; }
    }
}
