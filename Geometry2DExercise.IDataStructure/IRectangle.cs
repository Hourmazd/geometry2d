﻿namespace Geometry2DExercise.IDataStructure
{
    public interface IRectangle : IShape
    {
        IPoint Point1 { get; set; }
        IPoint Point2 { get; }
        IPoint Point3 { get; set; }
        IPoint Point4 { get; }
    }
}
