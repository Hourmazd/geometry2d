﻿using Geometry2DExercise.Shared;

namespace Geometry2DExercise.IDataStructure.BaseClasses
{
    public interface IDTOBase
    {
        #region Propperties

        ObjectStates State { get; }

        bool IsNew { get; }

        #endregion

        #region Methods

        void SetAdded();
        void ResetRemoved();
        void SetDirty();

        IDTOBase Clone();
        
        void BeginInitiate();
        void EndInitiate();

        string SerializeToXML();

        #endregion
    }
}
