﻿
namespace Geometry2DExercise.IDataStructure.BaseClasses
{
    public interface IVector
    {
        double X { get; set; }
        double Y { get; set; }
        double Magnitude { get; }

        void Normalize();

        double DotProduct(IVector vector);
    }
}
