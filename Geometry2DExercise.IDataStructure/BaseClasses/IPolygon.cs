﻿using System.Collections.Generic;

namespace Geometry2DExercise.IDataStructure.BaseClasses
{
    public interface IPolygon
    {
        IList<IVector> Edges { get; }

        IList<IVector> Points { get; }

        void CreatePolygon(IList<IVector> points);
    }
}
